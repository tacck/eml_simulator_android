/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator;

import net.tacck.app.android.eml_simulator.util.ViewUtil;
import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Memory List Adapter.
 */
public class MemoryListAdapter extends BaseAdapter {

    /** Context. */
    private Context mContext = null;
    /** Memory Array. */
    private SparseArray<MemoryListItem> mMemoryArray = null;
    /** View Holder. */
    private ViewHolder holder = null;

    /**
     * Constructor.
     * 
     * @param context Context
     * @param array Memory Array
     */
    public MemoryListAdapter(final Context context, final SparseArray<MemoryListItem> array) {
        super();

        mContext = context;
        mMemoryArray = array;
    }

    @Override
    public final View getView(final int position, final View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = new MemoryListItemView(mContext);

            holder = new ViewHolder();
            holder.address = (TextView) view.findViewById(R.id.textViewLabelMemory);
            holder.value = (TextView) view.findViewById(R.id.textViewMemory);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        MemoryListItem item = getItem(position);

        holder.address.setText(ViewUtil.decimalToHexString(item.getAddress()));
        holder.value.setText(ViewUtil.decimalToBinaryString(item.getValue()));

        return view;
    }

    @Override
    public final int getCount() {
        int ret = 0;

        if (mMemoryArray != null) {
            ret = mMemoryArray.size();
        }

        return ret;
    }

    @Override
    public final MemoryListItem getItem(final int position) {
        MemoryListItem ret = null;

        if (mMemoryArray != null) {
            ret = mMemoryArray.get(position);
        }

        return ret;
    }

    @Override
    public final long getItemId(final int position) {
        // do nothing.
        return 0;
    }

    /**
     * View Holder.
     */
    private static class ViewHolder {
        /** Address. */
        public TextView address = null;
        /** Value. */
        public TextView value = null;
    }
}
