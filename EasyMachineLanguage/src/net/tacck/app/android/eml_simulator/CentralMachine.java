/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator;

import net.tacck.app.android.eml_simulator.command.Commands;
import net.tacck.app.android.eml_simulator.util.CommandUtil;

/**
 * Central Machine.
 */
public final class CentralMachine {

    /** Operation first byte index. */
    public static final short OPERATION_FIRST_BYTE = 0;
    /** Operation second byte index. */
    public static final short OPERATION_SECOND_BYTE = 1;
    /** Register A index. */
    public static final short REGISTER_A = 0;

    /** Max memory address. */
    private static final short MAX_ADDRESS = 0xFF;
    /** Max operation size. */
    private static final short MAX_OPERATION_SIZE = 2;
    /** Max register counts. */
    private static final short MAX_REGISTER_COUNTS = 1;

    /** Singleton instance. */
    private static CentralMachine sInstance = null;

    /** Operations. */
    private byte[] mOperations = new byte[MAX_OPERATION_SIZE];
    /** Registers. */
    private byte[] mRegisters = new byte[MAX_REGISTER_COUNTS];
    /** Overflow counter. */
    private boolean mOverflow = false;
    /** Program counter. */
    private byte mProgramCounter = 0;
    /** Memory. */
    private byte[] mMemory = new byte[MAX_ADDRESS + 1];

    /** Fetch Next Flag. */
    private boolean mFetchNextFlag = false;

    /**
     * Constructor.
     */
    private CentralMachine() {
        clearRegistersAndCounters();
    }

    /**
     * Get singleton instance.
     * 
     * @return CentralMachine
     */
    public static CentralMachine getInstance() {
        if (sInstance == null) {
            sInstance = new CentralMachine();
        }
        return sInstance;
    }

    /**
     * Clear all registers and counters.
     */
    public void clearRegistersAndCounters() {
        for (int i = 0; i < mOperations.length; i++) {
            mOperations[i] = 0;
        }
        for (int i = 0; i < mRegisters.length; i++) {
            mRegisters[i] = 0;
        }
        mOverflow = false;
        mProgramCounter = 0;
    }

    /**
     * Clear memory.
     */
    public void clearMemory() {
        for (int i = 0; i < mMemory.length; i++) {
            mMemory[i] = 0;
        }
    }

    /**
     * Reset all memory.
     */
    public void reset() {
        clearRegistersAndCounters();
        clearMemory();
    }

    /**
     * Fetch Operation from memory.
     */
    public void fetchOperation() {
        fetchOperation(OPERATION_FIRST_BYTE);
        mOperations[OPERATION_SECOND_BYTE] = 0;
    }

    /**
     * Fetch Operation from memory.
     * 
     * @param index Store Index
     * @throws IllegalArgumentException
     */
    public void fetchOperation(final int index) {
        if (index >= MAX_OPERATION_SIZE) {
            throw new IllegalArgumentException();
        }

        int pc = CommandUtil.byteToInt(mProgramCounter);
        mOperations[index] = (byte) mMemory[pc];
        mProgramCounter++;
    }

    /**
     * Execute Operation.
     * 
     * @return positive value: Success negative value: Fail
     *         zero: Control Command END
     */
    public int execOperation() {
        int execRet = -1;

        try {
            if (mFetchNextFlag) {
                // Currently, Operations' size is 2 bytes.
                fetchOperation(OPERATION_SECOND_BYTE);
                mFetchNextFlag = false;

                execRet = Commands.exec(sInstance);
            } else {
                fetchOperation();

                int size = Commands.getCodeSize(mOperations[OPERATION_FIRST_BYTE]);
                if (size <= 0) {
                    // Not Function.
                    execRet = -1;
                } else if (size > 1) {
                    mFetchNextFlag = true;
                    execRet = 1;
                } else {
                    // size == 1
                    execRet = Commands.exec(sInstance);
                }
            }
        } catch (IllegalArgumentException e) {
            // Not Function.
            execRet = -1;
        }

        return execRet;
    }

    // // Getter Setter // //

    /**
     * Get Operations.
     * 
     * @return Operations
     */
    public byte[] getOperations() {
        return mOperations;
    }

    /**
     * Get Operation Code.
     * 
     * @param index Operation Index
     * @return Operation Code
     * @throws IllegalArgumentException
     */
    public byte getOperationCode(final int index) {
        byte ret = (byte) 0x00;
        if ((index >= 0) && (index < MAX_OPERATION_SIZE)) {
            ret = mOperations[index];
        } else {
            throw new IllegalArgumentException();
        }

        return ret;
    }

    /**
     * Set Operation Code.
     * 
     * @param index Operation Index
     * @param value Operation Code
     * @throws IllegalArgumentException
     */
    public void setOperationCode(final int index, final byte value) {
        if ((index >= 0) && (index < MAX_OPERATION_SIZE)) {
            mOperations[index] = value;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Get Registers.
     * 
     * @return Registers
     */
    public byte[] getRegister() {
        return mRegisters;
    }

    /**
     * Get Register Value.
     * 
     * @param index Register Index
     * @return Register Value
     * @throws IllegalArgumentException
     */
    public byte getRegisterValue(final int index) {
        byte ret = (byte) 0x00;

        if ((index >= 0) && (index < MAX_REGISTER_COUNTS)) {
            ret = mRegisters[index];
        } else {
            throw new IllegalArgumentException();
        }

        return ret;
    }

    /**
     * Set Register Value.
     * 
     * @param index Register Index
     * @param value Register Value
     * @throws IllegalArgumentException
     */
    public void setRegisterValue(final int index, final byte value) {
        if ((index >= 0) && (index < MAX_REGISTER_COUNTS)) {
            mRegisters[index] = value;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Check Over Flow Flag.
     * 
     * @return overflow
     */
    public boolean isOverflow() {
        return mOverflow;
    }

    /**
     * Set Over Flow Flag.
     * 
     * @param overflow Over Flow Flag
     */
    public void setOverflow(final boolean overflow) {
        mOverflow = overflow;
    }

    /**
     * Get Program Counter.
     * 
     * @return programCounter
     */
    public byte getProgramCounter() {
        return mProgramCounter;
    }

    /**
     * Set Program Counter.
     * 
     * @param programCounter Program Counter
     */
    public void setProgramCounter(final byte programCounter) {
        mProgramCounter = programCounter;
    }

    /**
     * Set Previous Program Counter.
     */
    public void setPreviousProgramCounter() {
        int val = CommandUtil.byteToInt(mProgramCounter);
        if (val > 0) {
            int previousVal = val - 1;
            mProgramCounter = CommandUtil.intToByte(previousVal);
        }
    }

    /**
     * Get Memory.
     * 
     * @return memory
     */
    public byte[] getMemory() {
        return mMemory;
    }

    /**
     * Get Memory Value.
     * 
     * @param address Address
     * @return Memory Value
     */
    public byte getMemoryValue(final byte address) {
        return mMemory[CommandUtil.byteToInt(address)];
    }

    /**
     * Set Memory Value.
     * 
     * @param address Address
     * @param value Memory Value
     */
    public void setMemoryValue(final byte address, final byte value) {
        mMemory[CommandUtil.byteToInt(address)] = value;
    }
}
