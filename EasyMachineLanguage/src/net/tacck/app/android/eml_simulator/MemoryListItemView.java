/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

/**
 * View Class for MemoryListItem.
 */
public class MemoryListItemView extends FrameLayout implements Checkable {

    /** Checked. */
    private boolean mChecked = false;
    /** Value TextView. */
    private TextView mTextViewValue = null;

    /**
     * Constructor.
     * 
     * @param context Context
     */
    public MemoryListItemView(final Context context) {
        this(context, null);
    }

    /**
     * Constructor.
     * 
     * @param context Context
     * @param attrs Attrs
     */
    public MemoryListItemView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Constructor.
     * 
     * @param context Context
     * @param attrs Attrs
     * @param defStyle DefStyle
     */
    public MemoryListItemView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /**
     * Initialize.
     */
    private void init() {
        // Set ListView.LayoutParams.
        setLayoutParams(new ListView.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

        View view = LayoutInflater.from(getContext()).inflate(R.layout.memory_list_item, this,
                false);
        addView(view);

        mTextViewValue = (TextView) view.findViewById(R.id.textViewMemory);
    }

    @Override
    public final boolean isChecked() {
        return mChecked;
    }

    @Override
    public final void setChecked(final boolean checked) {
        mChecked = checked;
        mTextViewValue.setSelected(checked);
    }

    @Override
    public final void toggle() {
        setChecked(!mChecked);
    }
}
