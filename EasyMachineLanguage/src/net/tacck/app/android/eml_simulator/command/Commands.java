/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator.command;

import java.util.HashMap;
import java.util.Map;

import net.tacck.app.android.eml_simulator.CentralMachine;
import net.tacck.app.android.eml_simulator.util.CommandUtil;

/**
 * Commands.
 */
public final class Commands {

    /** Program Code Size Map. */
    private static Map<Byte, Integer> mSizeMap = null;

    /**
     * Constructor.
     */
    private Commands() {
    }

    /**
     * Initialize.
     */
    private static void initSizeMap() {
        mSizeMap = new HashMap<Byte, Integer>();
        mSizeMap.put(End.OPERATION_END, End.OPERATION_CODE_SIZE_END);
        mSizeMap.put(Add.OPERATION_ADD_REGA_AND_DATA_TO_REGA,
                Add.OPERATION_CODE_SIZE_ADD_REGA_AND_DATA_TO_REGA);
        mSizeMap.put(Sub.OPERATION_SUB_DATA_FROM_REGA_TO_REGA,
                Sub.OPERATION_CODE_SIZE_SUB_DATA_FROM_REGA_TO_REGA);
        mSizeMap.put(Lsh.OPERATION_LSH_FROM_REGA_TO_REGA,
                Lsh.OPERATION_CODE_SIZE_LSH_FROM_REGA_TO_REGA);
        mSizeMap.put(Rsh.OPERATION_RSH_FROM_REGA_TO_REGA,
                Rsh.OPERATION_CODE_SIZE_RSH_FROM_REGA_TO_REGA);
        mSizeMap.put(Str.OPERATION_STR_FROM_REGA_TO_DATA,
                Str.OPERATION_CODE_SIZE_STR_FROM_REGA_TO_DATA);
        mSizeMap.put(Lod.OPERATION_LOD_FROM_DATA_TO_REGA,
                Lod.OPERATION_CODE_SIZE_LOD_FROM_DATA_TO_REGA);
        mSizeMap.put(And.OPERATION_AND_REGA_AND_DATA_TO_REGA,
                And.OPERATION_CODE_SIZE_AND_REGA_AND_DATA_TO_REGA);
        mSizeMap.put(Or.OPERATION_OR_REGA_AND_DATA_TO_REGA,
                Or.OPERATION_CODE_SIZE_OR_REGA_AND_DATA_TO_REGA);
        mSizeMap.put(Xor.OPERATION_XOR_REGA_AND_DATA_TO_REGA,
                Xor.OPERATION_CODE_SIZE_XOR_REGA_AND_DATA_TO_REGA);
        mSizeMap.put(Jmp.OPERATION_JMP_PROGRAM, Jmp.OPERATION_CODE_SIZE_JMP_PROGRAM);
        mSizeMap.put(Jrz.OPERATION_JRZ_PROGRAM_FROM_REGA,
                Jrz.OPERATION_CODE_SIZE_JRZ_PROGRAM_FROM_REGA);
        mSizeMap.put(Jov.OPERATION_JOV_PROGRAM_FROM_OVERFLOW,
                Jov.OPERATION_CODE_SIZE_JOV_PROGRAM_FROM_OVERFLOW);
    }

    /**
     * Get Program Code Size.
     * 
     * @param programCode Program Code
     * @return Program Code Size
     */
    public static int getCodeSize(final byte programCode) {
        if (mSizeMap == null) {
            initSizeMap();
        }

        Integer size = mSizeMap.get(programCode);

        if (size == null) {
            return 0;
        }
        return size;
    }

    /**
     * Execute Program Code(s).
     * 
     * @param centralMachine CentralMachine
     * @return positive value: Success negative value: Fail
     *         zero: Control Command END
     */
    public static int exec(final CentralMachine centralMachine) {
        int ret = 1;
        boolean execRet = true;
        byte operator = CommandUtil.getOperator(centralMachine
                .getOperationCode(CentralMachine.OPERATION_FIRST_BYTE));

        switch (operator) {
            case End.OPERATOR_CODE:
                execRet = End.exec(centralMachine);
                ret = 0;
                break;

            case Add.OPERATOR_CODE:
                execRet = Add.exec(centralMachine);
                break;

            case Sub.OPERATOR_CODE:
                execRet = Sub.exec(centralMachine);
                break;

            case Lsh.OPERATOR_CODE:
                execRet = Lsh.exec(centralMachine);
                break;

            case Rsh.OPERATOR_CODE:
                execRet = Rsh.exec(centralMachine);
                break;

            case Str.OPERATOR_CODE:
                execRet = Str.exec(centralMachine);
                break;

            case Lod.OPERATOR_CODE:
                execRet = Lod.exec(centralMachine);
                break;

            case And.OPERATOR_CODE:
                execRet = And.exec(centralMachine);
                break;

            case Or.OPERATOR_CODE:
                execRet = Or.exec(centralMachine);
                break;

            case Xor.OPERATOR_CODE:
                execRet = Xor.exec(centralMachine);
                break;

            case Jmp.OPERATOR_CODE:
                execRet = Jmp.exec(centralMachine);
                break;

            case Jrz.OPERATOR_CODE:
                execRet = Jrz.exec(centralMachine);
                break;

            case Jov.OPERATOR_CODE:
                execRet = Jov.exec(centralMachine);
                break;

            default:
                execRet = false;
                break;
        }

        if (!execRet) {
            ret = -1;
        }

        return ret;
    }
}
