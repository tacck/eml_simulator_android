/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator.command;

import net.tacck.app.android.eml_simulator.CentralMachine;

/**
 * End Command.
 */
public final class End extends CommandBase {

    /** Operator Code. */
    public static final byte OPERATOR_CODE = 0x00;

    /** Operation Code: End. */
    public static final byte OPERATION_END = 0x00;
    /** Operation Code Size: End. */
    public static final int OPERATION_CODE_SIZE_END = 1;

    /**
     * Constructor.
     */
    private End() {
    }

    /**
     * Execute Program Code(s).
     * 
     * @param centralMachine CentralMachine
     * @return true: Success false: Fail
     */
    public static boolean exec(final CentralMachine centralMachine) {
        boolean ret = true;

        byte[] operation = centralMachine.getOperations();
        // byte[] registers = centralMachine.getRegister();
        // byte[] memory = centralMachine.getMemory();

        byte programCode = operation[CentralMachine.OPERATION_FIRST_BYTE];
        if (!check(programCode, OPERATOR_CODE)) {
            return false;
        }

        switch (operation[CentralMachine.OPERATION_FIRST_BYTE]) {
            case OPERATION_END:
                // do nothing.
                break;

            default:
                // Not Function.
                ret = false;
                break;
        }

        return ret;
    }
}
