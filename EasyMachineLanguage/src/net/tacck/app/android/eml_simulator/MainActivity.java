/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator;

import net.tacck.app.android.eml_simulator.util.CommandUtil;
import net.tacck.app.android.eml_simulator.util.ViewUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Main Activity.
 */
public class MainActivity extends Activity implements OnClickListener, OnItemClickListener {

    /** Each Memory Length. */
    private static final int EACH_MEMORY_LENGTH = 0x80;
    /** Data Memory Offset. */
    private static final int DATA_MEMORY_OFFSET = 0x80;

    /** Central Machine. */
    private CentralMachine mCentralMachine = null;

    /** Program Memory Array. */
    private SparseArray<MemoryListItem> mProgramMemoryArray = null;
    /** Program Memory List Adapter. */
    private MemoryListAdapter mProgramMemoryListAdapter = null;
    /** Data Memory Array. */
    private SparseArray<MemoryListItem> mDataMemoryArray = null;
    /** Data Memory List Adapter. */
    private MemoryListAdapter mDataMemoryListAdapter = null;

    /** Information Area: Operation first byte. */
    private TextView mOperationFirst = null;
    /** Information Area: Operation second byte. */
    private TextView mOperationSecond = null;
    /** Information Area: Register A. */
    private TextView mRegisterA = null;
    /** Information Area: Overflow. */
    private TextView mOverflow = null;
    /** Information Area: Program Counter. */
    private TextView mProgramCounter = null;

    /** List Area: Program Memory List. */
    private ListView mProgramMemoryListView = null;
    /** List Area: Data Memory List. */
    private ListView mDataMemoryListView = null;

    /** Control Area: Step Button. */
    private ImageButton mStepButton = null;
    /** Control Area: Stop Button. */
    private ImageButton mStopButton = null;
    /** Control Area: Execute Button. */
    private ImageButton mExecButton = null;
    /** Control Area: Pause Button. */
    private ImageButton mPauseButton = null;
    /** Control Area: Reset Button. */
    private ImageButton mResetButton = null;

    /** Memory List Item Input Dialog: Dialog View. */
    private View mItemInputView = null;
    /** Memory List Item Input EditText: EditText. */
    private EditText mItemInputEditText = null;
    /** Memory List Item Input Button: Button 0. */
    private Button mItemInput0Button = null;
    /** Memory List Item Input Button: Button 1. */
    private Button mItemInput1Button = null;
    /** Memory List Item Input Button: Clear. */
    private ImageButton mItemInputClearButton = null;

    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Central Machine.
        mCentralMachine = CentralMachine.getInstance();

        // Information Items.
        mOperationFirst = (TextView) findViewById(R.id.textViewOperationFirst);
        mOperationSecond = (TextView) findViewById(R.id.textViewOperationSecond);
        mRegisterA = (TextView) findViewById(R.id.textViewRegisterA);
        mOverflow = (TextView) findViewById(R.id.textViewOverflow);
        mProgramCounter = (TextView) findViewById(R.id.textViewProgramCounter);

        // Lists and Arrays.
        mProgramMemoryArray = new SparseArray<MemoryListItem>();
        mProgramMemoryListView = (ListView) findViewById(R.id.listViewProgramMemory);
        mProgramMemoryListAdapter = new MemoryListAdapter(this, mProgramMemoryArray);
        mProgramMemoryListView.setAdapter(mProgramMemoryListAdapter);
        mProgramMemoryListView.setOnItemClickListener(this);

        mDataMemoryArray = new SparseArray<MemoryListItem>();
        mDataMemoryListView = (ListView) findViewById(R.id.listViewDataMemory);
        mDataMemoryListAdapter = new MemoryListAdapter(this, mDataMemoryArray);
        mDataMemoryListView.setAdapter(mDataMemoryListAdapter);
        mDataMemoryListView.setOnItemClickListener(this);

        initMemoryList();
        clearInformationsAndMemory();

        // Control Buttons.
        mStopButton = (ImageButton) findViewById(R.id.buttonStop);
        mStopButton.setOnClickListener(this);

        mStepButton = (ImageButton) findViewById(R.id.buttonStep);
        mStepButton.setOnClickListener(this);

        mExecButton = (ImageButton) findViewById(R.id.buttonExec);
        mExecButton.setOnClickListener(this);

        mPauseButton = (ImageButton) findViewById(R.id.buttonPause);
        mPauseButton.setOnClickListener(this);

        mResetButton = (ImageButton) findViewById(R.id.buttonReset);
        mResetButton.setOnClickListener(this);

        // Memory Item Input View.
        mItemInputView = LayoutInflater.from(this).inflate(R.layout.list_item_input, null);
        mItemInputEditText = (EditText) mItemInputView.findViewById(R.id.editTextListItemInput);
        mItemInput0Button = (Button) mItemInputView.findViewById(R.id.buttonListItemInput0);
        mItemInput0Button.setOnClickListener(this);
        mItemInput1Button = (Button) mItemInputView.findViewById(R.id.buttonListItemInput1);
        mItemInput1Button.setOnClickListener(this);
        mItemInputClearButton = (ImageButton) mItemInputView
                .findViewById(R.id.imageButtonListItemInputClear);
        mItemInputClearButton.setOnClickListener(this);
    }

    @Override
    public final boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /**
     * Get Central Machine.
     * 
     * @return Central Machine
     */
    public final CentralMachine getCentralMachine() {
        return mCentralMachine;
    }

    /**
     * Initialization for Memory Lists.
     */
    public final void initMemoryList() {
        int length = EACH_MEMORY_LENGTH;
        for (int i = 0; i < length; i++) {
            MemoryListItem item = new MemoryListItem(i, 0);
            mProgramMemoryArray.put(i, item);
        }
        for (int i = 0; i < length; i++) {
            MemoryListItem item = new MemoryListItem(i + DATA_MEMORY_OFFSET, 0);
            mDataMemoryArray.put(i, item);
        }
    }

    /**
     * Clear Informations and All Memory Lists.
     */
    public final void clearInformationsAndMemory() {
        clearInformations();
        cleardMemory();
    }

    /**
     * Clear Informations.
     */
    public final void clearInformations() {
        mCentralMachine.clearRegistersAndCounters();

        updateInformations();
    }

    /**
     * Clear All Memory Lists.
     */
    public final void cleardMemory() {
        mCentralMachine.clearMemory();

        clearMemoryListViewSelected();
        updateProgramMemoryListAdapter();
        updateDataMemoryListAdapter();
    }

    /**
     * Set mProgramMemoryArray item.
     * 
     * @param position Position
     * @param value Value
     */
    public final void setProgramMemoryArrayItem(final int position, final byte value) {
        if ((position >= 0x00) && (position < EACH_MEMORY_LENGTH)) {
            byte address = CommandUtil.intToByte(position);
            mCentralMachine.setMemoryValue(address, value);

            MemoryListItem item = mProgramMemoryArray.get(position);
            item.setValue(value);
            mProgramMemoryArray.put(position, item);
        }
    }

    /**
     * Update mProgramMemoryListAdapter.
     */
    public final void updateProgramMemoryListAdapter() {
        byte[] memory = mCentralMachine.getMemory();
        int length = EACH_MEMORY_LENGTH;

        for (int i = 0; i < length; i++) {
            MemoryListItem item = mProgramMemoryArray.get(i);
            item.setValue(memory[i]);
        }

        mProgramMemoryListAdapter.notifyDataSetChanged();
    }

    /**
     * Set mDataMemoryArray item.
     * 
     * @param position Position
     * @param value Value
     */
    public final void setDataMemoryArrayItem(final int position, final byte value) {
        if ((position >= 0x00) && (position < EACH_MEMORY_LENGTH)) {
            byte address = CommandUtil.intToByte(position + DATA_MEMORY_OFFSET);
            mCentralMachine.setMemoryValue(address, value);

            MemoryListItem item = mDataMemoryArray.get(position);
            item.setValue(value);
            mDataMemoryArray.put(position, item);
        }
    }

    /**
     * Update mDataMemoryListAdapter.
     */
    public final void updateDataMemoryListAdapter() {
        byte[] memory = mCentralMachine.getMemory();
        int length = EACH_MEMORY_LENGTH;

        for (int i = 0; i < length; i++) {
            MemoryListItem item = mDataMemoryArray.get(i);
            item.setValue(memory[i + DATA_MEMORY_OFFSET]);
        }

        mDataMemoryListAdapter.notifyDataSetChanged();
    }

    /**
     * Execution.
     */
    public final void exec() {
        int ret = mCentralMachine.execOperation();
        if (ret == 0) {
            // END Execute.
            mCentralMachine.setPreviousProgramCounter();
            Toast.makeText(this, R.string.exec_end, Toast.LENGTH_SHORT).show();
        } else if (ret < 0) {
            // Show exec error.
            mCentralMachine.setPreviousProgramCounter();
            Toast.makeText(this, R.string.exec_err, Toast.LENGTH_SHORT).show();
        }

        updateInformations();
    }

    /**
     * Update Informations.
     */
    public final void updateInformations() {
        // Values.
        byte[] operations = mCentralMachine.getOperations();
        byte[] registers = mCentralMachine.getRegister();
        byte programCounter = mCentralMachine.getProgramCounter();

        mOperationFirst.setText(getStringValue(operations[CentralMachine.OPERATION_FIRST_BYTE]));
        mOperationSecond.setText(getStringValue(operations[CentralMachine.OPERATION_SECOND_BYTE]));
        mRegisterA.setText(getStringValue(registers[CentralMachine.REGISTER_A]));
        mOverflow.setText(mCentralMachine.isOverflow() ? "1" : "0");
        mProgramCounter.setText(getStringValue(programCounter));
    }

    /**
     * Get String Value.
     * 
     * @param value Value
     * @return String Value
     */
    private String getStringValue(final int value) {
        String str = null;

        // TODO: Select BinaryString or HexString.
        str = ViewUtil.decimalToBinaryString(value);

        return str;
    }

    @Override
    public final void onClick(final View v) {
        if (mStopButton.equals(v)) {
            clearInformations();
        } else if (mStepButton.equals(v)) {
            exec();
        } else if (mResetButton.equals(v)) {
            clearInformationsAndMemory();
        } else if (mItemInput0Button.equals(v)) {
            String text = mItemInputEditText.getText() + "0";
            mItemInputEditText.setText(text);
        } else if (mItemInput1Button.equals(v)) {
            String text = mItemInputEditText.getText() + "1";
            mItemInputEditText.setText(text);
        } else if (mItemInputClearButton.equals(v)) {
            mItemInputEditText.setText(null);
        }
    }

    @Override
    public final void onItemClick(final AdapterView<?> parent, final View view, final int position,
            final long id) {
        // Guard for illegal event.
        if (!mProgramMemoryListView.equals(parent) && !mDataMemoryListView.equals(parent)) {
            return;
        }

        clearMemoryListViewSelected();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        ListView listView = null;
        MemoryListAdapter adapter = null;
        SparseArray<MemoryListItem> array = null;

        if (mProgramMemoryListView.equals(parent)) {
            listView = mProgramMemoryListView;
            adapter = mProgramMemoryListAdapter;
            array = mProgramMemoryArray;
        } else {
            listView = mDataMemoryListView;
            adapter = mDataMemoryListAdapter;
            array = mDataMemoryArray;
        }

        listView.setItemChecked(position, true);
        MemoryListItem item = array.get(position);

        // Set dialog EditText.
        mItemInputEditText.setText(ViewUtil.decimalToBinaryString(CommandUtil.byteToInt(item
                .getValue())));

        // Clear view relations.
        ViewGroup itemInputViewParent = (ViewGroup) mItemInputView.getParent();
        if (itemInputViewParent != null) {
            itemInputViewParent.removeAllViews();
        }

        // Build Dialog items.
        final ListView finalListView = listView;
        final MemoryListAdapter finalAdapter = adapter;
        builder.setTitle(
                getString(R.string.list_item_input_address)
                        + ViewUtil.decimalToHexString(CommandUtil.byteToInt(item.getAddress())))
                .setView(mItemInputView)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        finalListView.setItemChecked(position, false);
                        byte val = ViewUtil.binaryStringToByte(mItemInputEditText.getText()
                                .toString());

                        if (mProgramMemoryListView.equals(parent)) {
                            setProgramMemoryArrayItem(position, val);
                        } else if (mDataMemoryListView.equals(parent)) {
                            setDataMemoryArrayItem(position, val);
                        }

                        finalAdapter.notifyDataSetChanged();
                    }
                }) //
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        finalListView.setItemChecked(position, false);
                    }
                }) //
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(final DialogInterface dialog) {
                        finalListView.setItemChecked(position, false);
                    }
                });
        builder.show();

    }

    /**
     * Clear MemoryListView selected.
     */
    private void clearMemoryListViewSelected() {
        int prevPosition = mProgramMemoryListView.getCheckedItemPosition();
        if (prevPosition != AdapterView.INVALID_POSITION) {
            mProgramMemoryListView.setItemChecked(prevPosition, false);
        }

        prevPosition = mDataMemoryListView.getCheckedItemPosition();
        if (prevPosition != AdapterView.INVALID_POSITION) {
            mDataMemoryListView.setItemChecked(prevPosition, false);
        }
    }
}
