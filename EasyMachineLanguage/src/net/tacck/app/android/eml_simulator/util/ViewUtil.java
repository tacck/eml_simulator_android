/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator.util;

import java.text.DecimalFormat;

import android.text.TextUtils;
import android.util.Log;

/**
 * Utility Class for View.
 */
public final class ViewUtil {
    /** LOGTAG. */
    private static final String LOGTAG = "ViewUtil";

    /** Binary Format. */
    private static final String BINARY_FORMAT = "00000000";

    /**
     * Constructor.
     */
    private ViewUtil() {
    }

    /**
     * Decimal to Binary String.
     * 
     * @param value Value
     * @return Binary String
     */
    public static String decimalToBinaryString(final int value) {
        DecimalFormat df = new DecimalFormat(BINARY_FORMAT);
        String str = df.format(Long.decode(Integer.toBinaryString(CommandUtil
                .intToByteSizeInt(value))));

        return str;
    }

    /**
     * Binary String to Byte Value.
     * 
     * @param binaryString Binary String
     * @return Byte Value
     */
    public static byte binaryStringToByte(final String binaryString) {
        byte ret = 0;

        if (!TextUtils.isEmpty(binaryString)) {
            try {
                ret = (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & Integer
                        .valueOf(binaryString, 2));
            } catch (NumberFormatException e) {
                Log.e(LOGTAG, "NumberFormatException.");
            }
        }

        return ret;
    }

    /**
     * Decimal to Hex String.
     * 
     * @param value Value
     * @return Hex String
     */
    public static String decimalToHexString(final int value) {
        String str = Integer.toHexString(CommandUtil.intToByteSizeInt(value));
        if ((str.length() % 2) == 1) {
            str = "0" + str;
        }
        str = str.toUpperCase();

        return str;
    }
}
