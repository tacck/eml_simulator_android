/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.eml_simulator.util;

/**
 * Utility Class for Command Operation.
 */
public final class CommandUtil {

    /** Byte Unsigned Max Value. */
    public static final int BYTE_UNSIGNED_MAX_VALUE = 0xFF;

    /** Operator Mask. */
    private static final byte OPERATOR_MASK = (byte) 0xF0;
    /** Operand Mask. */
    // private static final byte OPERAND_MASK = (byte) 0x0F;
    /** Reference Mask. */
    // private static final byte REFERENCE_MASK = (byte) 0x0C;
    /** From Register Mask. */
    // private static final byte REGISTER_FROM_MASK = (byte) 0x02;
    /** To Register Mask. */
    // private static final byte REGISTER_TO_MASK = (byte) 0x01;

    /**
     * Constructor.
     */
    private CommandUtil() {
    }

    /**
     * Get Operator.
     * 
     * @param programCode Program Code
     * @return Operator
     */
    public static byte getOperator(final byte programCode) {
        return (byte) (OPERATOR_MASK & programCode);
    }

    /**
     * Get Operand.
     * 
     * @param programCode Program Code
     * @return Operand
     */
    // public static byte getOperand(final byte programCode) {
    // return (byte) (OPERAND_MASK & programCode);
    // }

    /**
     * Get Reference.
     * 
     * @param programCode Program Code
     * @return Reference
     */
    // public static byte getReference(final byte programCode) {
    // return (byte) (REFERENCE_MASK & programCode);
    // }

    /**
     * Get From Register.
     * 
     * @param programCode Program Code
     * @return From Register
     */
    // public static byte getRegisterFrom(final byte programCode) {
    // return (byte) (REGISTER_FROM_MASK & programCode);
    // }

    /**
     * Get To Register.
     * 
     * @param programCode Program Code
     * @return To Register
     */
    // public static byte getRegisterTo(final byte programCode) {
    // return (byte) (REGISTER_TO_MASK & programCode);
    // }

    /**
     * Get int value from byte value.
     * 
     * @param value byte value
     * @return int value
     */
    public static int byteToInt(final byte value) {
        return (int) (BYTE_UNSIGNED_MAX_VALUE & value);
    }

    /**
     * Get byte value from int value.
     * 
     * @param value int value
     * @return byte value
     */
    public static byte intToByte(final int value) {
        return (byte) (BYTE_UNSIGNED_MAX_VALUE & value);
    }

    /**
     * Get byte size int value from int value.
     * 
     * @param value int value
     * @return int value
     */
    public static int intToByteSizeInt(final int value) {
        return (int) (BYTE_UNSIGNED_MAX_VALUE & value);
    }
}
