/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.easymachinelanguage.test;

import net.tacck.app.android.eml_simulator.CentralMachine;
import net.tacck.app.android.eml_simulator.command.Add;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

/**
 * Test for CentralMachine class.
 */
@RunWith(RobolectricTestRunner.class)
public class CentralMachineTest {

    /**
     * Constructor.
     */
    public CentralMachineTest() {
    }

    /**
     * Test for getInstance.
     * 
     * @throws Exception
     */
    @Test
    public final void testGetInstance() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        CentralMachine machine2 = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        Assert.assertSame(machine, machine2);
    }

    /**
     * Test for clearMemory.
     * 
     * @throws Exception
     */
    @Test
    public final void testClearMemory() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        byte[] memory = machine.getMemory();
        memory[0] = (byte) 0xFF;
        memory[127] = (byte) 0xFF;
        memory[128] = (byte) 0xFF;
        memory[255] = (byte) 0xFF;

        Assert.assertEquals(memory[0], (byte) 0xFF);
        Assert.assertEquals(memory[127], (byte) 0xFF);
        Assert.assertEquals(memory[128], (byte) 0xFF);
        Assert.assertEquals(memory[255], (byte) 0xFF);

        machine.clearMemory();

        Assert.assertEquals(memory[0], (byte) 0x00);
        Assert.assertEquals(memory[127], (byte) 0x00);
        Assert.assertEquals(memory[128], (byte) 0x00);
        Assert.assertEquals(memory[255], (byte) 0x00);
    }

    /**
     * Test for clearRegistersAndCounters.
     * 
     * @throws Exception
     */
    @Test
    public final void testClearRegistersAndCounters() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        byte[] operations = machine.getOperations();
        byte[] registor = machine.getRegister();

        operations[CentralMachine.OPERATION_FIRST_BYTE] = (byte) 0xFF;
        operations[CentralMachine.OPERATION_SECOND_BYTE] = (byte) 0xFF;

        registor[CentralMachine.REGISTER_A] = (byte) 0xFF;

        machine.setOverflow(true);
        machine.setProgramCounter((byte) 0xFF);

        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], (byte) 0xFF);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], (byte) 0xFF);
        Assert.assertEquals(registor[CentralMachine.REGISTER_A], (byte) 0xFF);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0xFF);
        Assert.assertTrue(machine.isOverflow());

        machine.setPreviousProgramCounter();
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0xFE);
        machine.setProgramCounter((byte) 0x00);
        machine.setPreviousProgramCounter();
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x00);
        machine.setProgramCounter((byte) 0xFF);

        machine.clearRegistersAndCounters();

        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], (byte) 0x00);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], (byte) 0x00);
        Assert.assertEquals(registor[CentralMachine.REGISTER_A], (byte) 0x00);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x00);
        Assert.assertFalse(machine.isOverflow());
    }

    /**
     * Test for reset.
     * 
     * @throws Exception
     */
    @Test
    public final void testReset() throws Exception {
        //
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        // Memory
        byte[] memory = machine.getMemory();
        memory[0] = (byte) 0xFF;
        memory[127] = (byte) 0xFF;
        memory[128] = (byte) 0xFF;
        memory[255] = (byte) 0xFF;

        Assert.assertEquals(memory[0], (byte) 0xFF);
        Assert.assertEquals(memory[127], (byte) 0xFF);
        Assert.assertEquals(memory[128], (byte) 0xFF);
        Assert.assertEquals(memory[255], (byte) 0xFF);

        // Registers
        byte[] operations = machine.getOperations();
        byte[] registor = machine.getRegister();

        operations[CentralMachine.OPERATION_FIRST_BYTE] = (byte) 0xFF;
        operations[CentralMachine.OPERATION_SECOND_BYTE] = (byte) 0xFF;

        registor[CentralMachine.REGISTER_A] = (byte) 0xFF;

        machine.setOverflow(true);
        machine.setProgramCounter((byte) 0xFF);

        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], (byte) 0xFF);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], (byte) 0xFF);
        Assert.assertEquals(registor[CentralMachine.REGISTER_A], (byte) 0xFF);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0xFF);
        Assert.assertTrue(machine.isOverflow());

        machine.reset();

        // Clear memory
        Assert.assertEquals(memory[0], (byte) 0x00);
        Assert.assertEquals(memory[127], (byte) 0x00);
        Assert.assertEquals(memory[128], (byte) 0x00);
        Assert.assertEquals(memory[255], (byte) 0x00);

        // Clear registers
        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], (byte) 0x00);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], (byte) 0x00);
        Assert.assertEquals(registor[CentralMachine.REGISTER_A], (byte) 0x00);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x00);
        Assert.assertFalse(machine.isOverflow());
    }

    /**
     * Test for fetchOperation.
     * 
     * @throws Exception
     */
    @Test
    public final void testFetchOperation() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        byte[] memory = machine.getMemory();
        byte[] operations = machine.getOperations();
        // byte[] registor = machine.getRegister();

        memory[0x00] = (byte) 0xFF;
        memory[0x01] = (byte) 0x81;

        machine.fetchOperation();

        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], memory[0x00]);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], (byte) 0x00);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x01);

        machine.fetchOperation(CentralMachine.OPERATION_SECOND_BYTE);

        Assert.assertEquals(operations[CentralMachine.OPERATION_FIRST_BYTE], memory[0x00]);
        Assert.assertEquals(operations[CentralMachine.OPERATION_SECOND_BYTE], memory[0x01]);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x02);

        boolean exceptionFlag = false;
        try {
            machine.fetchOperation(3);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);
    }

    /**
     * Test for getOperationCode.
     * 
     * @throws Exception
     */
    @Test
    public final void testGetOperationCode() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        // byte[] memory = machine.getMemory();
        byte[] operations = machine.getOperations();
        // byte[] registor = machine.getRegister();

        byte firstByteValue = (byte) 0xFF;
        byte secondByteValue = (byte) 0x0F;

        operations[CentralMachine.OPERATION_FIRST_BYTE] = firstByteValue;
        operations[CentralMachine.OPERATION_SECOND_BYTE] = secondByteValue;

        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_FIRST_BYTE),
                firstByteValue);
        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_SECOND_BYTE),
                secondByteValue);

        boolean exceptionFlag = false;
        try {
            machine.getOperationCode(3);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);

        exceptionFlag = false;
        try {
            machine.getOperationCode(-1);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);
    }

    /**
     * Test for setOperationCode.
     * 
     * @throws Exception
     */
    @Test
    public final void testSetOperationCode() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        machine.reset();

        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_FIRST_BYTE),
                (byte) 0x00);
        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_SECOND_BYTE),
                (byte) 0x00);

        byte firstByteValue = (byte) 0xFF;
        byte secondByteValue = (byte) 0x0F;

        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE, firstByteValue);
        machine.setOperationCode(CentralMachine.OPERATION_SECOND_BYTE, secondByteValue);

        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_FIRST_BYTE),
                firstByteValue);
        Assert.assertEquals(machine.getOperationCode(CentralMachine.OPERATION_SECOND_BYTE),
                secondByteValue);

        boolean exceptionFlag = false;
        try {
            machine.setOperationCode(3, (byte) 0x00);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);

        exceptionFlag = false;
        try {
            machine.setOperationCode(-1, (byte) 0x00);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);
    }

    /**
     * Test for getRegisterValue.
     * 
     * @throws Exception
     */
    @Test
    public final void testGetRegisterValue() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        // byte[] memory = machine.getMemory();
        // byte[] operations = machine.getOperations();
        byte[] registor = machine.getRegister();

        byte regAValue = (byte) 0xFF;

        registor[CentralMachine.REGISTER_A] = regAValue;

        Assert.assertEquals(machine.getRegisterValue(CentralMachine.REGISTER_A), regAValue);

        boolean exceptionFlag = false;
        try {
            machine.getRegisterValue(2);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);

        exceptionFlag = false;
        try {
            machine.getRegisterValue(-1);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);
    }

    /**
     * Test for setRegisterValue.
     * 
     * @throws Exception
     */
    @Test
    public final void testSetRegisterValue() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        machine.reset();

        Assert.assertEquals(machine.getRegisterValue(CentralMachine.REGISTER_A), (byte) 0x00);

        byte regAValue = (byte) 0xFF;

        machine.setRegisterValue(CentralMachine.REGISTER_A, regAValue);

        Assert.assertEquals(machine.getRegisterValue(CentralMachine.REGISTER_A), regAValue);

        boolean exceptionFlag = false;
        try {
            machine.setRegisterValue(2, (byte) 0x00);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);

        exceptionFlag = false;
        try {
            machine.setRegisterValue(-1, (byte) 0x00);
        } catch (IllegalArgumentException e) {
            exceptionFlag = true;
        }
        Assert.assertTrue(exceptionFlag);
    }

    /**
     * Test for getMemoryValue.
     * 
     * @throws Exception
     */
    @Test
    public final void testGetMemoryValue() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        byte[] memory = machine.getMemory();
        // byte[] operations = machine.getOperations();
        // byte[] registor = machine.getRegister();

        memory[0] = (byte) 0xFF;
        memory[127] = (byte) 0xFF;
        memory[128] = (byte) 0xFF;
        memory[255] = (byte) 0xFF;

        Assert.assertEquals(machine.getMemoryValue((byte) 0x00), (byte) 0xFF);
        Assert.assertEquals(machine.getMemoryValue((byte) 0x7F), (byte) 0xFF);
        Assert.assertEquals(machine.getMemoryValue((byte) 0x80), (byte) 0xFF);
        Assert.assertEquals(machine.getMemoryValue((byte) 0xFF), (byte) 0xFF);

        byte val = machine.getMemoryValue((byte) -1);
        Assert.assertEquals(machine.getMemoryValue((byte) 0xFF), val);
    }

    /**
     * Test for setMemoryValue.
     * 
     * @throws Exception
     */
    @Test
    public final void testSetMemoryValue() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        machine.reset();

        byte[] memory = machine.getMemory();

        machine.setMemoryValue((byte) 0x00, (byte) 0xFF);
        machine.setMemoryValue((byte) 0x7F, (byte) 0xFF);
        machine.setMemoryValue((byte) 0x80, (byte) 0xFF);
        machine.setMemoryValue((byte) 0xFF, (byte) 0xFF);

        Assert.assertEquals(memory[0], (byte) 0xFF);
        Assert.assertEquals(memory[127], (byte) 0xFF);
        Assert.assertEquals(memory[128], (byte) 0xFF);
        Assert.assertEquals(memory[255], (byte) 0xFF);

        machine.setMemoryValue((byte) -1, (byte) 0x0F);
        Assert.assertEquals(memory[255], (byte) 0x0F);
    }

    /**
     * Test for execOperation.
     * 
     * @throws Exception
     */
    @Test
    public final void testExecOperation() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        int ret = 0;

        machine.reset();

        // int size =
        // Commands.getCodeSize(Jov.OPERATION_JOV_PROGRAM_FROM_OVERFLOW);
        // Assert.assertEquals(size,
        // Jov.OPERATION_CODE_SIZE_JOV_PROGRAM_FROM_OVERFLOW);
        // size = Commands.getCodeSize((byte) 0xFF);
        // Assert.assertEquals(size, 0);

        machine.setMemoryValue((byte) 0x00, (byte) 0xFF);
        ret = machine.execOperation();
        Assert.assertEquals(ret, -1);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x01);

        machine.clearRegistersAndCounters();

        byte value1 = Add.OPERATION_ADD_REGA_AND_DATA_TO_REGA;
        byte value2 = (byte) 0x80;
        byte value3 = (byte) 0xFF;
        machine.setMemoryValue((byte) 0x00, value1);
        machine.setMemoryValue((byte) 0x01, value2);
        machine.setMemoryValue((byte) 0x80, value3);

        ret = machine.execOperation();
        Assert.assertEquals(ret, 1);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x01);

        ret = machine.execOperation();
        Assert.assertEquals(ret, 1);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x02);

        ret = machine.execOperation();
        Assert.assertEquals(ret, 0);
        Assert.assertEquals(machine.getProgramCounter(), (byte) 0x03);
    }
}
