/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.easymachinelanguage.test;

import net.tacck.app.android.eml_simulator.util.ViewUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

/**
 * Test for ViewUtil class.
 */
@RunWith(RobolectricTestRunner.class)
public class ViewUtilTest {

    /**
     * Constructor.
     */
    public ViewUtilTest() {
    }

    /**
     * Test for decimalToBinaryString.
     * 
     * @throws Exception
     */
    @Test
    public final void testDecimalToBinaryString() throws Exception {
        String str = null;

        str = ViewUtil.decimalToBinaryString(0);
        Assert.assertTrue("00000000".equals(str));

        str = ViewUtil.decimalToBinaryString(3);
        Assert.assertTrue("00000011".equals(str));

        str = ViewUtil.decimalToBinaryString(10);
        Assert.assertTrue("00001010".equals(str));

        str = ViewUtil.decimalToBinaryString(16);
        Assert.assertTrue("00010000".equals(str));

        str = ViewUtil.decimalToBinaryString(128);
        Assert.assertTrue("10000000".equals(str));

        str = ViewUtil.decimalToBinaryString(255);
        Assert.assertTrue("11111111".equals(str));

        str = ViewUtil.decimalToBinaryString(256);
        Assert.assertTrue("00000000".equals(str));
    }

    /**
     * Test for decimalToHexString.
     * 
     * @throws Exception
     */
    @Test
    public final void testDecimalToHexString() throws Exception {
        String str = null;

        str = ViewUtil.decimalToHexString(0);
        Assert.assertTrue("00".equals(str));

        str = ViewUtil.decimalToHexString(3);
        Assert.assertTrue("03".equals(str));

        str = ViewUtil.decimalToHexString(10);
        Assert.assertTrue("0A".equals(str));

        str = ViewUtil.decimalToHexString(16);
        Assert.assertTrue("10".equals(str));

        str = ViewUtil.decimalToHexString(128);
        Assert.assertTrue("80".equals(str));

        str = ViewUtil.decimalToHexString(255);
        Assert.assertTrue("FF".equals(str));

        str = ViewUtil.decimalToHexString(256);
        Assert.assertTrue("00".equals(str));
    }

    /**
     * Test for binaryStringToByte.
     * 
     * @throws Exception
     */
    @Test
    public final void testBinaryStringToByte() throws Exception {
        String str = null;
        byte val = 0;

        str = "00000000";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == 0);

        str = "00000011";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == 3);

        str = "00001010";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == 10);

        str = "00010000";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == 16);

        str = "10000000";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == (byte) 128);

        str = "11111111";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == (byte) 255);

        val = ViewUtil.binaryStringToByte(null);
        Assert.assertTrue(val == (byte) 0);

        val = ViewUtil.binaryStringToByte("");
        Assert.assertTrue(val == (byte) 0);

        str = "1111a111";
        val = ViewUtil.binaryStringToByte(str);
        Assert.assertTrue(val == (byte) 0);
    }
}
