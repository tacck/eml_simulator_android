/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.easymachinelanguage.test;

import net.tacck.app.android.eml_simulator.CentralMachine;
import net.tacck.app.android.eml_simulator.command.Add;
import net.tacck.app.android.eml_simulator.command.And;
import net.tacck.app.android.eml_simulator.command.CommandBase;
import net.tacck.app.android.eml_simulator.command.Commands;
import net.tacck.app.android.eml_simulator.command.End;
import net.tacck.app.android.eml_simulator.command.Jmp;
import net.tacck.app.android.eml_simulator.command.Jov;
import net.tacck.app.android.eml_simulator.command.Jrz;
import net.tacck.app.android.eml_simulator.command.Lod;
import net.tacck.app.android.eml_simulator.command.Lsh;
import net.tacck.app.android.eml_simulator.command.Or;
import net.tacck.app.android.eml_simulator.command.Rsh;
import net.tacck.app.android.eml_simulator.command.Str;
import net.tacck.app.android.eml_simulator.command.Sub;
import net.tacck.app.android.eml_simulator.command.Xor;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

/**
 * Test for Commands class.
 */
@RunWith(RobolectricTestRunner.class)
public class CommandsTest {

    /**
     * Constructor.
     */
    public CommandsTest() {
    }

    /**
     * Test for getCodeSize.
     * 
     * @throws Exception
     */
    @Test
    public final void testGetCodeSize() throws Exception {
        int size = 0;

        size = Commands.getCodeSize((byte) 0xFF);
        Assert.assertEquals(0, size);

        size = Commands.getCodeSize(Add.OPERATION_ADD_REGA_AND_DATA_TO_REGA);
        Assert.assertEquals(Add.OPERATION_CODE_SIZE_ADD_REGA_AND_DATA_TO_REGA, size);
    }

    /**
     * Test for exec.
     * 
     * @throws Exception
     */
    @Test
    public final void testExec() throws Exception {
        CentralMachine machine = CentralMachine.getInstance();
        Assert.assertNotNull(machine);

        machine.reset();

        int ret = 0;

        // End
        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE,
                (byte) (End.OPERATOR_CODE | (byte) 0x0F));
        ret = Commands.exec(machine);
        Assert.assertEquals(-1, ret);
        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE, End.OPERATION_END);
        ret = Commands.exec(machine);
        Assert.assertEquals(0, ret);

        // Add
        testExecCommands(machine, Add.OPERATOR_CODE, Add.OPERATION_ADD_REGA_AND_DATA_TO_REGA);

        // Sub
        testExecCommands(machine, Sub.OPERATOR_CODE, Sub.OPERATION_SUB_DATA_FROM_REGA_TO_REGA);

        // Lsh
        testExecCommands(machine, (byte) (Lsh.OPERATOR_CODE | (byte) 0x0F),
                Lsh.OPERATION_LSH_FROM_REGA_TO_REGA);

        // Rsh
        testExecCommands(machine, (byte) (Rsh.OPERATOR_CODE | (byte) 0x0F),
                Rsh.OPERATION_RSH_FROM_REGA_TO_REGA);

        // Str
        testExecCommands(machine, Str.OPERATOR_CODE, Str.OPERATION_STR_FROM_REGA_TO_DATA);

        // Lod
        testExecCommands(machine, Lod.OPERATOR_CODE, Lod.OPERATION_LOD_FROM_DATA_TO_REGA);

        // And
        testExecCommands(machine, And.OPERATOR_CODE, And.OPERATION_AND_REGA_AND_DATA_TO_REGA);

        // Or
        testExecCommands(machine, Or.OPERATOR_CODE, Or.OPERATION_OR_REGA_AND_DATA_TO_REGA);

        // Xor
        testExecCommands(machine, Xor.OPERATOR_CODE, Xor.OPERATION_XOR_REGA_AND_DATA_TO_REGA);

        // Jmp
        testExecCommands(machine, Jmp.OPERATOR_CODE, Jmp.OPERATION_JMP_PROGRAM);

        // Jrz
        testExecCommands(machine, Jrz.OPERATOR_CODE, Jrz.OPERATION_JRZ_PROGRAM_FROM_REGA);

        // Jov
        testExecCommands(machine, Jov.OPERATOR_CODE, Jov.OPERATION_JOV_PROGRAM_FROM_OVERFLOW);

        // Not Function
        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE, (byte) 0xD0);
        ret = Commands.exec(machine);
        Assert.assertEquals(-1, ret);
    }

    /**
     * Common Exec Commands.
     * 
     * @param machine CentralMachine
     * @param operatorCode Operator Code
     * @param operator Operator
     */
    private void testExecCommands(CentralMachine machine, byte operatorCode, byte operator) {
        int ret = 0;

        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE, operatorCode);
        ret = Commands.exec(machine);
        Assert.assertEquals(-1, ret);
        machine.setOperationCode(CentralMachine.OPERATION_FIRST_BYTE, operator);
        ret = Commands.exec(machine);
        Assert.assertEquals(1, ret);
    }

    /**
     * Test for CommandBase class.
     * 
     * @throws Exception
     */
    @Test
    public void testCheckCommandBase() throws Exception {
        Assert.assertFalse(CheckCommand.exec(null));

        byte programCode = (byte) 0xFF;
        byte operatorCode = (byte) 0xF0;
        Assert.assertTrue(CheckCommand.callCheck(programCode, operatorCode));

        programCode = (byte) 0xEF;
        operatorCode = (byte) 0xF0;
        Assert.assertFalse(CheckCommand.callCheck(programCode, operatorCode));

        programCode = (byte) 0xFF;
        operatorCode = (byte) 0xE0;
        Assert.assertFalse(CheckCommand.callCheck(programCode, operatorCode));
    }

    /**
     * Test Class for CommandBase class.
     */
    private static class CheckCommand extends CommandBase {

        /**
         * Call check().
         * 
         * @param programCode Program Code
         * @param operatorCode Operator Code
         * @return check() result
         */
        public static boolean callCheck(final byte programCode, final byte operatorCode) {
            return CommandBase.check(programCode, operatorCode);
        }
    }
}
