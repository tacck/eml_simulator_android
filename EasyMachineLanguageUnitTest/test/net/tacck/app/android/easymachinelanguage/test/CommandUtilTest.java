/*
 * Copyright (c) 2012, Takuya KIHARA / Tacck.NET All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  1. Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation and/or
 *     other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.tacck.app.android.easymachinelanguage.test;

import net.tacck.app.android.eml_simulator.util.CommandUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

/**
 * Test for CommandUtil class.
 */
@RunWith(RobolectricTestRunner.class)
public class CommandUtilTest {

    /**
     * Constructor.
     */
    public CommandUtilTest() {
    }

    /**
     * Test for byteToInt.
     * 
     * @throws Exception
     */
    @Test
    public void testByteToInt() throws Exception {
        byte byteValue = 0;
        int intValue = 0;

        byteValue = 0;
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = 3;
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = 10;
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = 16;
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & 128);
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & 255);
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        byteValue = (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & 256);
        intValue = CommandUtil.byteToInt(byteValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));
    }

    /**
     * Test for intToByte.
     * 
     * @throws Exception
     */
    @Test
    public void testIntToByte() throws Exception {
        byte byteValue = 0;
        int intValue = 0;

        intValue = 0;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 3;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 10;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 16;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 128;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 255;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertEquals(intValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));

        intValue = 256;
        byteValue = CommandUtil.intToByte(intValue);
        Assert.assertFalse(intValue == (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));
        Assert.assertEquals(0, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & byteValue));
    }

    /**
     * Test for intToByteSizeInt.
     * 
     * @throws Exception
     */
    @Test
    public void testIntToByteSizeInt() throws Exception {
        int inValue = 0;
        int outValue = 0;

        inValue = 0;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 3;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 10;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 16;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 128;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 255;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertEquals(inValue, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 256;
        outValue = CommandUtil.intToByteSizeInt(inValue);
        Assert.assertFalse(inValue == (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));
        Assert.assertEquals(0, (int) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));
    }

    /**
     * Test for getOperator.
     * 
     * @throws Exception
     */
    @Test
    public void testGetOperator() throws Exception {
        byte inValue = 0;
        byte outValue = 0;
        byte exValue = 0;

        inValue = 0x00;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 0x0F;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 0x10;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = 0x1F;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = (byte) 0x80;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = (byte) 0x8F;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));

        inValue = (byte) 0xFF;
        exValue = (byte) (0xF0 & inValue);
        outValue = CommandUtil.getOperator(inValue);
        Assert.assertEquals(exValue, (byte) (CommandUtil.BYTE_UNSIGNED_MAX_VALUE & outValue));
    }
}
